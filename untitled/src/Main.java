import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число от 1 до 5: ");
        int userInput = scanner.nextInt();
        switch (userInput) {
            case 1:
                System.out.println("Вы ввели число один");
                break;
            case 2:
                System.out.println("Вы ввели число два");
                break;
            case 3:
                System.out.println("Вы ввели число три");
                break;
            case 4:
                System.out.println("Вы ввели число четыре");
                break;
            case 5:
                System.out.println("Вы ввели число пять");
                break;
            default:
                System.out.println("Вы ввели неизвестное число");
        }
        scanner.close();
    }
}